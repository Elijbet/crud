import React, { useState } from 'react';
import './App.css';

function App() {
  const [names, setNames] = useState([
    {
      id: 1,
      name: 'Chick',
      surname: 'Webb',
    },
    {
      id: 2,
      name: 'Django', 
      surname: 'Reinhardt',
    },
    {
      id: 3,
      name: 'Ella',
      surname: 'Fitzgerald',
    },
    {
      id: 4,
      name: 'George',
      surname: 'Gershwin',
    },
    {
      id: 5,
      name: 'Cole',
      surname: 'Porter',
    }
  ]);
  const [selection, setSelection] = useState('')
  const [prefix, setPrefix] = useState('')
  const [enteredName, setEnteredName] = useState({id: names.length, name: '', surname: ''})

  const getValueInput = event => {
    const prefix = event.target.value.toLowerCase()
    setPrefix(prefix);
    filter(prefix);
  }

  const filter = prefix => {
    let result = names.filter(
        person => person.surname.toLowerCase().startsWith(prefix) 
      );
    result.length === 1 ? setSelection(result[0]) : setSelection('')
  }

  const update = () => {
    if(selection){
      const localNames = [...names]
      const index = localNames.findIndex(person => person.id === selection.id)
      localNames[index] = {id: localNames[index].id, name: enteredName.name, surname: enteredName.surname}
      setNames(localNames)
    } else {
      setNames([...names, {...enteredName}])
    }
  }

  const del = () => {
    let localNames = [...names].filter(person => person.id !== selection.id)
    setNames(localNames)
  }

  return (
    <div className="container" style={{display: 'flex', 'flexDirection': 'column'}}>
      <div className="top-panel">
        <div className="left"> 
          <div className='prefix'>
            <h4 style={{color: '#afbacb'}} className='label'>Filter Prefix</h4>
            <input value={prefix} onChange={getValueInput}/>
          </div>
          <div className='twrap' style={{margin: '10px', 'overflowY': 'scroll'}}>
            {
              names.map(person => (  
                <h4 
                  style={{color: '#afbacb', 'textAlign': 'left', margin: '0.5rem'}} 
                  className={selection.surname === person.surname ? 'selected' : ''}
                  key={person.id}
                >
                  {person.surname}, {person.name}
                </h4>
              ))
            }
          </div>
        </div>
        <div className="right">
          <div className='prefix'>
            <h4 className='label'>Name</h4>
            <input 
              style={{background: 'white', width: '80%', color: '#afbacb'}} 
              className='color'
              onChange={(event) => {setEnteredName({...enteredName, name: event.target.value, id: names.length + 1})}}
            />
          </div>
          <div className='prefix'>
            <h4 className='label'>Surname</h4>
            <input 
              style={{background: 'white', width: '80%', color: '#afbacb'}} 
              className='color'
              onChange={(event) => {setEnteredName({...enteredName, surname: event.target.value, id: names.length + 1})}}
            />
          </div>
        </div>
      </div>
      <div className="bottom-panel"> 
        <button onClick={update}><h4>Create</h4></button>
        <button 
          className={selection ? '' : 'disabled'} 
          disabled={selection ? false : true}
          onClick={update}
        >
          <h4>
            Update
          </h4>
        </button>
        <button 
          className={selection ? '' : 'disabled'} 
          disabled={selection ? false : true}
          onClick={del}
        >
          <h4>
            Delete
          </h4>
        </button>
      </div>
    </div>
  );
}

export default App;